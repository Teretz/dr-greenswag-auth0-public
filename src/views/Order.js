import React, { Fragment } from "react";

import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ThumbDown from '@material-ui/icons/ThumbDown';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import BorderColor from '@material-ui/icons/BorderColor';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import ThumbUp from '@material-ui/icons/ThumbUp';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import PoAddBatchHead from "../components/PoAddBatchHead";
import PoAddBatchHeadResult from "../components/PoAddBatchHeadResult";
import PoAddBatchLine from "../components/PoAddBatchLine";
import PoAddBatchText from '../components/PoAddBatchText';
import PoAddBatchAddress from "../components/PoAddBatchAddress";
import PoAddBatchHeadChg from "../components/PoAddBatchHeadChg";
import Content from "../components/Content";

import PoConfirm from '../components/PoConfirm';
import PoAuthCreditCard from '../components/PoAuthCreditCard';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
  }));
  
  export default function Order() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
  
    return (
        <Fragment>
        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              <Tab label="Batch Head" icon={<AddShoppingCartIcon  />} {...a11yProps(0)} />
              <Tab label="Batch Line" icon={<BorderColor />} {...a11yProps(1)} />
              <Tab label="Batch Head - Charge" icon={<LocalShippingIcon  />} {...a11yProps(2)} />
              <Tab label="Batch Address" icon={<ContactMailIcon  />} {...a11yProps(3)} />
              <Tab label="Credit Auth" icon={<CreditCardIcon  />} {...a11yProps(4)} />
              <Tab label="Text Email" icon={<AlternateEmailIcon  />} {...a11yProps(5)} />
              <Tab label="Confirm" icon={<ThumbUp />} {...a11yProps(6)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <PoAddBatchHead />
            
          </TabPanel>
          <TabPanel value={value} index={1}>
            <PoAddBatchLine />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <PoAddBatchHeadChg />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <PoAddBatchAddress />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <PoAuthCreditCard />
          </TabPanel>
          <TabPanel value={value} index={5}>
            <PoAddBatchText />
          </TabPanel>
          <TabPanel value={value} index={6}>
            <PoConfirm />
          </TabPanel>
        </div>
        <hr />
    <Content />
        </Fragment>
      );
    }


