import React , { useState, useEffect }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

const PoAddBatchHeadResult = props => {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [profileState, setProfileState] = useState(props);
  const [cuno, setCuno] = useState(0);
  const [cuor, setCuor] = useState(0);
  const [faci, setFaci] = useState(0);


  const [orno, setOrno] = useState(0);
    const [itno, setItno] = useState(0);
    const [orqt, setOrqt] = useState(0);
    const [whlo, setWhlo] = useState(0);

  const [error, setError] = useState(0);


  const [persons, setPersons] = useState(0);
  
  

  useEffect(() => {
    setProfileState(props);
    console.log( global.orno )
    if (orno !== global.orno){
     setOrno(global.orno);
    }
    if (itno !== global.itno){
      setItno(global.itno);
     }
     if (orqt !== global.orqt){
      setOrqt(global.orqt);
     }
     if (whlo !== global.whlo){
      setWhlo(global.whlo);
     }
    
     console.log( global.orno )
    //axios.get(`https://jsonplaceholder.typicode.com/users`)
    var url = 'http://127.0.0.1:2080/OIS100MI/AddBatchLine;?CONO=' + 1 + '&ORNO=' + orno + '&ITNO='+ itno + '&ORQT=' + orqt + '&WHLO=' + whlo // + global.cuno;
     console.log( url)
    axios.get(url ,  {
      auth: {
        username: 'M3USRJOBS',
        password: '#RP7!sz2k9Pq'
      }
    }).then(res => {
      console.log(res);
      if(res.statusText === "OK"){
        console.log(res.data.Message);
        setError( res.data.Message )
        if(typeof res.data.Message === "undefined"){
          
          //SUCCESS
          console.log('ORDER PLACED');
          setError('Order Placed');
        }
      }
    })

  },[props]);

  
  

  return (
    <Card className={classes.root} id="po-batch-head-result" variant="outlined">
      <CardContent>
        ORNO: { orno } <br/>
        ITNO: { itno } <br/>
        ORQT: { orqt } <br/>
        WHLO: { whlo } <br/>
        ERROR: { error } <br/>
        
      </CardContent>
      <CardActions>
        
      </CardActions>
    </Card>
  );
}

export default PoAddBatchHeadResult;