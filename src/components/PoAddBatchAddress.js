import React, {Component, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import PoAddBatchAddressResult from './PoAddBatchAddressResult';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function PoAddBatchAddress() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const [state, setState] = useState({
    name: 'Param',
    email: 'param@gmail.com',
    cuno: 1,
    cuor: 2,
    faci: 3,
    orno: 3,
    itno: 3,
    orqt: 3,
    whlo: 3,
  });
  const [cuno, setCuno] = useState(0);
    const [cuor, setCuor] = useState(0);
    const [faci, setFaci] = useState(0);

    const [orno, setOrno] = useState(0);
    const [itno, setItno] = useState(0);
    const [adrt, setAdrt] = useState(0);
    const [cunm, setCunm] = useState(0);
    const [cua1, setCua1] = useState(0);
    const [cua2, setCua2] = useState(0);
    const [cua3, setCua3] = useState(0);
    const [cua4, setCua4] = useState(0);
    const [pono, setPono] = useState(0);
    const [cscd, setCscd] = useState(0);
    const [ecar, setEcar] = useState(0);
    const [modl, setModl] = useState(0);
    const [tedl, setTedl] = useState(0);
    const [tel2, setTel2] = useState(0);
    const [town, setTown] = useState(0);


    const handleChange = () => {
      setState({
        name: 'Vennila',
        email: 'vennila@gmail.com',
        faci: faci,
        orno: orno,
        itno: itno,
        adrt: adrt,
        cunm: cunm,
        cua1: cua1,
        cua2: cua2,
        cua3: cua3,
        cua4: cua4,
        pono: pono,
        cscd: cscd,
        ecar: ecar,
        modl: modl,
        tedl: tedl,
        tel2: tel2,
        town: town,

      });
    };

    useEffect(() => {
      if (state.orno !== global.orno) {
        global.orno = state.orno;
      }
      if (state.itno !== global.itno) {
        global.itno = state.itno;
      }
      if (state.adrt !== global.adrt) {
        global.adrt = state.adrt;
      }
      if (state.cunm !== global.cunm) {
        global.cunm = state.cunm;
      }
      if (state.cua1 !== global.cua1) {
        global.cua1 = state.cua1;
      }
      if (state.cua2 !== global.cua2) {
        global.cua2 = state.cua2;
      }
      if (state.cua3 !== global.cua3) {
        global.cua3 = state.cua3;
      }
      if (state.cua4 !== global.cua4) {
        global.cua4 = state.cua4;
      }
      if (state.pono !== global.pono) {
        global.pono = state.pono;
      }
      if (state.cscd !== global.cscd) {
        global.cscd = state.cscd;
      }
      if (state.ecar !== global.ecar) {
        global.ecar = state.ecar;
      }
      if (state.modl !== global.modl) {
        global.modl = state.modl;
      }
      if (state.tedl !== global.tedl) {
        global.tedl = state.tedl;
      }
      if (state.tel2 !== global.tel2) {
        global.tel2 = state.tel2;
      }
      if (state.town !== global.town) {
        global.town = state.town;
      }
    });



  return (
    <Card className={classes.root} variant="outlined">
      <Grid container spacing={2}>
      <Grid item xs={3}></Grid>
      <CardContent>
      <form className={classes.root} noValidate autoComplete="off">
      
      <TextField disabled id="outlined-basic" label="Company" variant="outlined" value="001" />
      <TextField  onChange={event => setFaci(event.target.value)} id="outlined-basic" label="Facility" variant="outlined"  />
      <TextField  onChange={event => setOrno(event.target.value)} id="outlined-basic" label="Temp. Order Number" variant="outlined"  />
      <TextField  onChange={event => setAdrt(event.target.value)} id="outlined-basic" label="Address Type" variant="outlined"  />
      <TextField  onChange={event => setCunm(event.target.value)} id="outlined-basic" label="Customer Name" variant="outlined"  />
      <TextField  onChange={event => setCua1(event.target.value)} id="outlined-basic" label="Address Line 1" variant="outlined"  />
      <TextField  onChange={event => setCua2(event.target.value)} id="outlined-basic" label="Address Line 2" variant="outlined"  />
      <TextField  onChange={event => setCua3(event.target.value)} id="outlined-basic" label="Address Line 3" variant="outlined"  />
      <TextField  onChange={event => setCua4(event.target.value)} id="outlined-basic" label="Address Line 4" variant="outlined"  />
      <TextField  onChange={event => setPono(event.target.value)} id="outlined-basic" label="Postal Code" variant="outlined"  />
      <TextField  onChange={event => setCscd(event.target.value)} id="outlined-basic" label="Country" variant="outlined"  />
      <TextField  onChange={event => setEcar(event.target.value)} id="outlined-basic" label="Area / State" variant="outlined"  />
      <TextField  onChange={event => setModl(event.target.value)} id="outlined-basic" label="Delivery Method" variant="outlined"  />
      <TextField  onChange={event => setTedl(event.target.value)} id="outlined-basic" label="Delivery Terms" variant="outlined"  />
      <TextField  onChange={event => setTel2(event.target.value)} id="outlined-basic" label="Terms Text" variant="outlined"  />
      <TextField  onChange={event => setTown(event.target.value)} id="outlined-basic" label="City" variant="outlined"  />

      

    </form>
        
        
      </CardContent>
      
        <Grid item xs={9}>
          <PoAddBatchAddressResult {...state} />
        </Grid>
      </Grid>
      <CardActions>
      <Button onClick={handleChange}  
        size="small">SEND REQUEST</Button>
      </CardActions>
    </Card>
  );
}