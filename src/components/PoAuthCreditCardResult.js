import React , { useState, useEffect }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

const PoAuthCreditCardResult = props => {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [profileState, setProfileState] = useState(props);
  const [cuno, setCuno] = useState(0);
  const [cono, setCono] = useState(0);
  const [nref, setNref] = useState(0);
  const [orno, setOrno] = useState(0);

  const [error, setError] = useState(0);


  const [persons, setPersons] = useState(0);
  
  

  useEffect(() => {
    setProfileState(props);
    console.log( global.cuno )
    if (cuno !== global.cuno){
     setCuno(global.cuno);
    }
    if (cono !== global.cono){
      setCono(global.cono);
    }
    if (orno !== global.orno){
      setOrno(global.orno);
    }
    if (nref !== global.nref){
      setNref(global.nref);
    }

    //axios.get(`https://jsonplaceholder.typicode.com/users`)
    var url = 'http://127.0.0.1:2080/OIS100MI/AuthCreditCard;?CONO=' + 1 + '&ORNO=' + orno + '&NREF=' + nref // + global.cuno;

    axios.get(url ,  {
      auth: {
        username: 'M3USRJOBS',
        password: '#RP7!sz2k9Pq'
      }
    }).then(res => {
      console.log(res);
      if(res.statusText === "OK"){
        console.log(res.data.Message);
        setError( res.data.Message )
        if(typeof res.data.Message === "undefined"){
          
          //SUCCESS
          console.log('ORDER PLACED');
          setError('Order Placed');
        }
      }
    })

  },[props]);

  
  

  return (
    <Card className={classes.root} id="po-batch-head-result" variant="outlined">
      <CardContent>
        NREF: { nref } <br/>
        ORNO: { orno } <br/>
        ERROR: { error } <br/>
        
      </CardContent>
      <CardActions>
        
      </CardActions>
    </Card>
  );
}

export default PoAuthCreditCardResult;