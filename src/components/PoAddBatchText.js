import React, {Component, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import PoAddBatchTextResult from './PoAddBatchTextResult';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function PoAddBatchLine() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const [state, setState] = useState({
    name: 'Param',
    email: 'param@gmail.com',
    cuno: 1,
    cuor: 2,
    faci: 3,
    orno: 3,
    type: 3,
    tytr: 3,
    txhe: 3,
    parm: 3,
  });
  const [cuno, setCuno] = useState(0);
    const [cuor, setCuor] = useState(0);
    const [faci, setFaci] = useState(0);

    const [orno, setOrno] = useState(0);
    const [type, setType] = useState(0);
    const [tytr, setTytr] = useState(0);
    const [txhe, setTxhe] = useState(0);
    const [parm, setParm] = useState(0);

    const handleChange = () => {
      setState({
        name: 'Vennila',
        email: 'vennila@gmail.com',
        orno: orno,
        type: type,
        tytr: tytr,
        txhe: txhe,
        parm: parm,   
      });
    };

    useEffect(() => {
      if (state.orno !== global.orno) {
        global.orno = state.orno;
      }
      if (state.type !== global.type) {
        global.type = state.type;
      }
      if (state.tytr !== global.tytr) {
        global.tytr = state.tytr;
      }
      if (state.txhe !== global.txhe) {
        global.txhe = state.txhe;
      }
      if (state.parm !== global.parm) {
        global.parm = state.parm;
      }
    });



  return (
    <Card className={classes.root} variant="outlined">
      <Grid container spacing={2}>
      <Grid item xs={3}></Grid>
      <CardContent>
      <form className={classes.root} noValidate autoComplete="off">
      
      <TextField disabled id="outlined-basic" label="Company" variant="outlined" value="001" />
      <TextField  onChange={event => setOrno(event.target.value)} id="outlined-basic" label="Order Number" variant="outlined"  />
      <TextField  onChange={event => setType(event.target.value)} id="outlined-basic" label="Order Type" variant="outlined" />
      <TextField  onChange={event => setTxhe(event.target.value)} id="outlined-basic" label="Document Class" variant="outlined"  />
      <TextField  onChange={event => setTytr(event.target.value)} id="outlined-basic" label="Type of text" variant="outlined"  />
      <TextField  onChange={event => setParm(event.target.value)} id="outlined-basic" label="Text" variant="outlined"  />
      

    </form>
        
        
      </CardContent>
      
        <Grid item xs={9}>
          <PoAddBatchTextResult {...state} />
        </Grid>
      </Grid>
      <CardActions>
      <Button onClick={handleChange}  
        size="small">SEND REQUEST</Button>
      </CardActions>
    </Card>
  );
}