

import {TutProfile } from './TutProfile';


const App = () => {
    const [state, setState] = useState({
      name: 'Param',
      email: 'param@gmail.com',
    });
  
    const handleChange = () => {
      setState({
        name: 'Vennila',
        email: 'vennila@gmail.com',
      });
    };
  
    return (
      <div className="App">
        <TutProfile {...state} />
        <button onClick={handleChange}>Change Profile</button>
      </div>
    );
  };