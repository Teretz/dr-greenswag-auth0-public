import React, {Component, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

//import { StyleSheet, View, Alert, Platform } from 'react-native';

import PoAuthCreditCardResult from './PoAuthCreditCardResult';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        flexGrow: 1,
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


export default function PoAuthCreditCard() {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    const [state, setState] = useState({
        name: 'Param',
        email: 'param@gmail.com',
        cuno: 1,
        nref: 2,
        orno: 3,
      });
    const [cuno, setCuno] = useState(0);
    const [cuor, setCuor] = useState(0);
    const [orno, setOrno] = useState(0);
    const [nref, setNref] = useState(0);

    const handleChange = () => {
        setState({
          name: 'Vennila',
          email: 'vennila@gmail.com',
          cuno: cuno,
          cuor: cuor,
          orno: orno,
          nref: nref,
        });
      };
    

    useEffect(() => {
        if (state.cuno !== global.cuno){
            global.cuno = state.cuno;
        }
        if (state.cuor !== global.cuor){
            global.cuor = state.cuor;
        }
        if (state.orno !== global.orno){
            global.orno = state.orno;
        }
        if (state.nref !== global.nref){
          global.nref = state.nref;
      }
      });
    

  return (
      
    <Card className={classes.root} id="po-batch-head-inputs" variant="outlined">
        <Grid container spacing={2}>
        <Grid item xs={3}>

        <CardContent>
      <form className={classes.root} noValidate autoComplete="off">
      <TextField disabled id="outlined-basic" label="Company" variant="outlined" value="001" />
      <TextField onChange={event => setOrno(event.target.value)} id="outlined-basic" label="Customer Number" variant="outlined"  />
      <TextField onChange={event => setNref(event.target.value)} id="outlined-basic" label="Reference Number" variant="outlined" />
      
    </form>
      </CardContent>
      
        </Grid>
        <Grid item xs={9}>
          <PoAuthCreditCardResult {...state} />
        </Grid>
      </Grid>
      <CardActions>
        
        
        <Button onClick={handleChange}  
        size="small">SEND REQUEST</Button>
      </CardActions>
      
    </Card>
    
  );
}


class Actions extends Component {
   
    SecondClassFunction=()=>{
   
      //Alert.alert("Second Class Function Without Argument Called");
        console.log('Second Class Function Without Argument Called  ')
    }
   
    SecondClassFunctionWithArgument=(Value)=>{
   
      //Alert.alert(Value);
      console.log(Value)
   
    }
   
  }