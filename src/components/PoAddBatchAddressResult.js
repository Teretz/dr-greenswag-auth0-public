import React , { useState, useEffect }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

const PoAddBatchHeadResult = props => {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [profileState, setProfileState] = useState(props);
  const [cuno, setCuno] = useState(0);
  const [cuor, setCuor] = useState(0);
  const [orno, setOrno] = useState(0);
  const [adrt, setAdrt] = useState(0);
  const [itno, setItno] = useState(0);
  const [faci, setFaci] = useState(0);
  const [cunm, setCunm] = useState(0);
  const [cua1, setCua1] = useState(0);
  const [cua2, setCua2] = useState(0);
  const [cua3, setCua3] = useState(0);
  const [cua4, setCua4] = useState(0);
  const [pono, setPono] = useState(0);
  const [cscd, setCscd] = useState(0);
  const [ecar, setEcar] = useState(0);
  const [modl, setModl] = useState(0);
  const [tedl, setTedl] = useState(0);
  const [tel2, setTel2] = useState(0);
  const [town, setTown] = useState(0);


  const [error, setError] = useState(0);


  const [persons, setPersons] = useState(0);
  
  

  useEffect(() => {
    setProfileState(props);
    console.log( global.cuno )
    if (cuno !== global.cuno){
     setCuno(global.cuno);
    }
    if (cuor !== global.cuor){
      setCuor(global.cuor);
    }
    if (faci !== global.faci){
      setFaci(global.faci);
    }
    if (adrt !== global.adrt){
      setAdrt(global.adrt);
    }
    if (orno !== global.orno){
      setOrno(global.orno);
     }
     if (cunm !== global.cunm){
      setCunm(global.cunm);
    }
     if (cua1 !== global.cua1){
       setCua1(global.cua1);
     }
    if (cua2 !== global.cua2){
      setCua2(global.cua2);
    }
    if (cua3 !== global.cua3){
      setCua3(global.cua3);
    }
    if (cua4 !== global.cua4){
      setCua4(global.cua4);
    }
    if (pono !== global.pono){
      setPono(global.pono);
    }
    if (cscd !== global.cscd){
      setCscd(global.cscd);
    }
    if (ecar !== global.ecar){
      setEcar(global.ecar);
    }
    if (modl !== global.modl){
      setModl(global.modl);
    }
    if (tedl !== global.tedl){
      setTedl(global.tedl);
    }
    if (tel2 !== global.tel2){
      setTel2(global.tel2);
    }
    if (town !== global.town){
      setTown(global.town);
    }


     

    //axios.get(`https://jsonplaceholder.typicode.com/users`)
    var url = 'http://127.0.0.1:2080/OIS100MI/AddBatchAddress;?CONO=' + 1  
    + '&FACI='+ faci + '&ORNO=' + orno + '&ADRT=' + adrt  + '&CUNM=' + cunm  
    + '&CUA1=' + cua1 + '&CUA2=' + cua2 + '&CUA3=' + cua3 + '&CUA4=' + cua4
    + '&PONO=' + pono + '&CSCD=' + cscd + '&ECAR=' + ecar + '&MODL=' + modl
    + '&TEDL=' + tedl + '&TEL2=' + tel2 + '&TOWN=' + town   

    axios.get(url ,  {
      auth: {
        username: 'M3USRJOBS',
        password: '#RP7!sz2k9Pq'
      }
    }).then(res => {
      console.log(res);
      if(res.statusText === "OK"){
        console.log(res.data.Message);
        setError( res.data.Message )
        if(typeof res.data.Message === "undefined"){
          
          //SUCCESS
          console.log('ORDER PLACED');
          setError('Order Placed');
        }
      }
    })

  },[props]);

  
  

  return (
    <Card className={classes.root} id="po-batch-head-result" variant="outlined">
      <CardContent>
        FACI: { faci } <br/>
        ORNO: { orno } <br/>
        CUNM: { cunm } <br/>
        CUA1: { cua1 } <br/>
        CUA2: { cua2 } <br/>
        CUA3: { cua3 } <br/>
        CUA4: { cua4 } <br/>
        PONO: { pono } <br/>
        CSCS: { cscd } <br/>
        ECAR: { ecar } <br/>
        MODL: { modl } <br/>
        TEDL: { tedl } <br/>
        TEL2: { tel2 } <br/>
        TOWN: { town } <br/>
        ERROR: { error } <br/>
        
      </CardContent>
      <CardActions>
        
      </CardActions>
    </Card>
  );
}

export default PoAddBatchHeadResult;