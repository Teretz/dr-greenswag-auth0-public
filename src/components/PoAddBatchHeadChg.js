import React, {Component, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

//import { StyleSheet, View, Alert, Platform } from 'react-native';

import PoAddBatchHeadChgResult from './PoAddBatchHeadChgResult';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        flexGrow: 1,
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


export default function PoAddBatchHeadChg() {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    const [state, setState] = useState({
        name: 'Param',
        email: 'param@gmail.com',
        cuno: 1,
        orno: 3,
        crid: 4,
        cram: 4,
      });
    const [cuno, setCuno] = useState(0);
    const [cono, setCono] = useState(0);
    const [crid, setCrid] = useState(0);
    const [orno, setOrno] = useState(0);
    const [cram, setCram] = useState(0);
    const [crfa, setCrfa] = useState(0);

    const handleChange = () => {
        setState({
          name: 'Vennila',
          email: 'vennila@gmail.com',
          cuno: cuno,
          cono: cono,
          orno: orno,
          crid: crid,
          cram: cram,
          crfa: crfa,
        });
      };
    

    useEffect(() => {
        if (state.cuno !== global.cuno){
            global.cuno = state.cuno;
        }
        if (state.cono !== global.cono){
            global.cono = state.cono;
        }
        if (state.orno !== global.orno){
            global.orno = state.orno;
        }
        if (state.crid !== global.crid){
          global.crid = state.crid;
        }
        if (state.cram !== global.cram){
          global.cram = state.cram;
        }
        if (state.crfa !== global.crfa){
          global.crfa = state.crfa;
        }
      });
    

  return (
      
    <Card className={classes.root} id="po-batch-head-inputs" variant="outlined">
        <Grid container spacing={2}>
        <Grid item xs={3}>

        <CardContent>
      <form className={classes.root} noValidate autoComplete="off">
      <TextField disabled id="outlined-basic" label="Company" variant="outlined" value="001" />
      <TextField onChange={event => setOrno(event.target.value)} id="outlined-basic" label="Order Number" variant="outlined"  />
      <TextField onChange={event => setCrid(event.target.value)} id="outlined-basic" label="Charge" variant="outlined" />
      <TextField onChange={event => setCram(event.target.value)} id="outlined-basic" label="Charge Amount" variant="outlined" />
      <TextField onChange={event => setCrfa(event.target.value)} id="outlined-basic" label="Calculation Factor" variant="outlined" />
      
    </form>
      </CardContent>
      
        </Grid>
        <Grid item xs={9}>
          <PoAddBatchHeadChgResult {...state} />
        </Grid>
      </Grid>
      <CardActions>
        
        
        <Button onClick={handleChange}  
        size="small">SEND REQUEST</Button>
      </CardActions>
      
    </Card>
    
  );
}


class Actions extends Component {
   
    SecondClassFunction=()=>{
   
      //Alert.alert("Second Class Function Without Argument Called");
        console.log('Second Class Function Without Argument Called  ')
    }
   
    SecondClassFunctionWithArgument=(Value)=>{
   
      //Alert.alert(Value);
      console.log(Value)
   
    }
   
  }