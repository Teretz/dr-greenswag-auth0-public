import React, {Component, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import PoAddBatchLineResult from './PoAddBatchLineResult';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function PoAddBatchLine() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const [state, setState] = useState({
    name: 'Param',
    email: 'param@gmail.com',
    cuno: 1,
    cuor: 2,
    faci: 3,
    orno: 3,
    itno: 3,
    orqt: 3,
    whlo: 3,
  });
  const [cuno, setCuno] = useState(0);
    const [cuor, setCuor] = useState(0);
    const [faci, setFaci] = useState(0);

    const [orno, setOrno] = useState(0);
    const [itno, setItno] = useState(0);
    const [orqt, setOrqt] = useState(0);
    const [whlo, setWhlo] = useState(0);

    const handleChange = () => {
      setState({
        name: 'Vennila',
        email: 'vennila@gmail.com',
        orno: orno,
        itno: itno,
        orqt: orqt,
        whlo: whlo,
      });
    };

    useEffect(() => {
      if (state.orno !== global.orno){
          global.orno = state.orno;
      }
      if (state.itno !== global.itno){
        global.itno = state.itno;
    }
    if (state.orqt !== global.orqt){
      global.orqt = state.orqt;
  }
  if (state.whlo !== global.whlo){
    global.whlo = state.whlo;
}
    });



  return (
    <Card className={classes.root} variant="outlined">
      <Grid container spacing={2}>
      <Grid item xs={3}></Grid>
      <CardContent>
      <form className={classes.root} noValidate autoComplete="off">
      
      <TextField disabled id="outlined-basic" label="Company" variant="outlined" value="001" />
      <TextField  onChange={event => setOrno(event.target.value)} id="outlined-basic" label="Temp. Order Number" variant="outlined"  />
      <TextField  onChange={event => setItno(event.target.value)} id="outlined-basic" label="Item Number" variant="outlined" />
      <TextField  onChange={event => setOrqt(event.target.value)} id="outlined-basic" label="Quantity" variant="outlined"  />
      <TextField  onChange={event => setWhlo(event.target.value)} id="outlined-basic" label="Warehouse" variant="outlined"  />
      

    </form>
        
        
      </CardContent>
      
        <Grid item xs={9}>
          <PoAddBatchLineResult {...state} />
        </Grid>
      </Grid>
      <CardActions>
      <Button onClick={handleChange}  
        size="small">SEND REQUEST</Button>
      </CardActions>
    </Card>
  );
}